const sql = require("./db.js");
const crypto = require('crypto');

// constructor
class Welcomer {
    constructor(welcomer) {
        this.name = welcomer.name;
        this.pseudo = welcomer.pseudo;
        this.password = welcomer.password;
        this.specality = welcomer.specality;
        this.promo = welcomer.promo;
        this.openingHour = welcomer.openingHour;
        this.closingHour = welcomer.closingHour;
        this.address = welcomer.address;
        this.description = welcomer.description;
        this.phone = welcomer.phone;
        this.mobilePhone = welcomer.mobilePhone;
        this.FK_location = welcomer.FK_location;
        this.FK_typeWelcomer = welcomer.FK_typeWelcomer;
    }

    static create(newWelcomer, result) {
        let hash = crypto.createHash('sha512');
        //passing the data to be hashed
        let data = hash.update(newWelcomer.password, 'utf-8');
        //Creating the hash in the required format
        let gen_hash = data.digest('hex');
        //Printing the output on the console
        newWelcomer.password = gen_hash;
        console.log(gen_hash);

        sql.query("INSERT INTO welcomers SET ?", newWelcomer, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            console.log("created welcomer: ", { id: res.insertId, ...newWelcomer });
            result(null, { id: res.insertId, ...newWelcomer });
        });
    }

    static findById(welcomerId, result) {
        sql.query(`SELECT * FROM welcomers WHERE idWelcomers = ${welcomerId}`, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if (res.length) {
                console.log("found welcomer: ", res[0]);
                result(null, res[0]);
                return;
            }

            // not found Welcomer with the id
            result({ kind: "not_found" }, null);
        });
    }

    static findAll({ name, type, zip, typeCuisine }, result) {
        let nameLike = ``;
        let selectClause = [`SELECT w.name, 
                                w.openingHour, 
                                w.closingHour,
                                w.address,
                                CONCAT(l.zipCode, ' ', l.city) AS 'place',
                                tw.name AS 'type',
                                tc.name AS 'typeCuisine',
                                w.FK_Location,
                                idTypeCuisine,
                                w.FK_TypeWelcomer`];
        const fromClause = `FROM welcomers w`;
        let joinClause = [`INNER JOIN typewelcomers tw ON w.FK_TypeWelcomer = tw.idTypeWelcomers
                            LEFT JOIN type_typecuisine  ttc ON w.idWelcomers = ttc.FK_Welcomer
                            LEFT JOIN typecuisine tc ON ttc.FK_TypeCuisine = tc.idTypeCuisine 
                            INNER JOIN locations l ON w.FK_Location = l.idLocation`];
        let whereClause = [];

        if (name || type || zip || typeCuisine) {
            if (name) {
                console.log('here');
                nameLike = `%${name}%`;
                whereClause.push({ clause: `w.name LIKE ?`, value: nameLike, op: "AND" });
            }
            if (type) {
                whereClause.push({ clause: `w.FK_TypeWelcomer = ?`, value: type, op: 'AND' });
            }
            if (typeCuisine) {
                whereClause.push({ clause: `tc.idTypeCuisine = ?`, value: typeCuisine, op: 'AND' });
            }
            if (zip) {
                whereClause.push({ clause: `w.FK_Location = ?`, value: zip, op: "AND" });
            }
        }
        let where = whereClause.length > 0 ? "WHERE " : "";
        const whereParam = [];

        for (const index in whereClause) {
            const clause = whereClause[index];
            if (whereClause.length > 1) {
                if (index != whereClause.length - 1) {
                    where += whereClause[index].clause + whereClause[index].op + " "
                } else {
                    where += `${clause.clause}`;
                }
            }
            else {
                where += `${clause.clause}`;
            }

            whereParam.push(clause.value);
        }

        sql.query(`${selectClause} ${fromClause} ${joinClause} ${where}`, whereParam, (err, res) => {

            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            console.log("welcomerssAll: ", res);
            result(null, res);
        });
    }

    static update(id, welcomer, result) {
        sql.query(
            `UPDATE welcomers SET 
                name = ?, 
                specality = ?, 
                promo = ?, 
                openingHour = ?, 
                closingHour = ?, 
                address = ?, 
                description = ?, 
                phone = ?, 
                mobilePhone = ?, 
                FK_location = ?, 
                FK_typeWelcomer = ? 
            WHERE idWelcomers = ?`,
            [welcomer.name, welcomer.specality, welcomer.promo, welcomer.openingHour, welcomer.closingHour,
            welcomer.address, welcomer.description, welcomer.phone, welcomer.mobilePhone,
            welcomer.FK_location, welcomer.FK_typeWelcomer, id],
            (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                    return;
                }

                if (res.affectedRows == 0) {
                    // not found Welcomer with the id
                    result({ kind: "not_found" }, null);
                    return;
                }

                console.log("updated welcomer: ", { id: id, ...welcomer });
                result(null, { id: id, ...welcomer });
            }
        );
    }

    static remove(id, result) {
        sql.query("DELETE FROM welcomers WHERE idWelcomer = ?", id, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Welcomer with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("deleted welcomer with id: ", id);
            result(null, res);
        });
    }
}

module.exports = Welcomer;
