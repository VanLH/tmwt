const sql = require("./db.js");
const crypto = require('crypto');

// constructor
class Outgoer {
    constructor(outgoer) {
        this.firstName = outgoer.firstName;
        this.lastName = outgoer.lastName;
        this.pseudo = outgoer.pseudo;
        this.address = outgoer.address;
        this.birthdate = outgoer.birthdate;
        this.password = outgoer.password;
        this.gender = outgoer.gender;
        this.FK_location = outgoer.FK_location;
    }
    static create(newOutgoer, result) {

        try {
            let hash = crypto.createHash('sha512');
            //passing the data to be hashed
            let data = hash.update(newOutgoer.password, 'utf-8');
            //Creating the hash in the required format
            let gen_hash = data.digest('hex');
            //Printing the output on the console
            newOutgoer.password = gen_hash;
            
            sql.query("INSERT INTO outgoers SET ?", newOutgoer, (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }

                console.log("created outgoer: ", { id: res.insertId, ...newOutgoer });
                result(null, { id: res.insertId, ...newOutgoer });
            });
        } catch (error) {
            console.log("error: ", error);
            result(error, null);
            return;
        }
    }
    static findById(outgoerId, result) {
        sql.query(`SELECT *, DATE_FORMAT(birthdate,'%d/%m/%Y') AS birthdate FROM outgoers WHERE idOutgoers = ?`,[outgoerId], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if (res.length) {
                console.log("found outgoer: ", res[0]);
                result(null, res[0]);
                return;
            }

            // not found Outgoer with the id
            result({ kind: "not_found" }, null);
        });
    }
    static findAll({ id, name, firstname, pseudo, zip }, result) {

        let nameLike = ``;
        let firstnameLike = ``;
        let pseudoLike = ``;
        let selectClause = [`SELECT o.firstName, o.lastName, o.pseudo, o.gender, l.city `];
        const fromClause = `FROM outgoers o `;
        let joinClause = [`JOIN locations l ON o.FK_Location = l.idLocation `];
        let whereClause = [`WHERE o.idOutgoers <> ? 
                            AND ( o.idOutgoers IN 
                                    (
                                        select f.FK_Outgoers1 
                                        from friends f 
                                        where f.FK_Outgoers1 = ? OR f.FK_Outgoers2 = ?
                                    )
                                OR o.idOutgoers in 
                                    (
                                        select f.FK_Outgoers2 
                                        from friends f 
                                        where f.FK_Outgoers1 = ? OR f.FK_Outgoers2 = ?
                                    )
                                )`
        ];

        if (name || firstname || zip || pseudo) {
            if (name) {
                nameLike = `%${name}%`;
                whereClause.push(`AND o.name LIKE ? `);
            }
            if (firstname) {
                firstnameLike = `%${firstname}%`;
                whereClause.push(`AND o.firstname LIKE ? `);
            }
            if (pseudo) {
                pseudoLike = `%${pseudo}%`;
                whereClause.push(`AND o.pseudo LIKE ? `);
            }
            if (zip) {
                whereClause.push(`AND o.FK_Location = ? `);
            }
        }
        sql.query(`${selectClause} ${fromClause} ${joinClause} ${whereClause}`, [id, id, id, id, id, name, firstname, pseudo, zip], (err, res) => {

            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            console.log("outgoersAll: ", res);
            result(null, res);
        });

    }
    static updateById(id, outgoer, result) {
        sql.query(
            `UPDATE outgoers SET 
                firstName = ?, 
                lastName = ?, 
                pseudo = ?, 
                address = ?, 
                birthdate = ?, 
                password = ?, 
                gender = ?, 
                FK_location = ?
            WHERE idOutgoers = ?`,
            [outgoer.firstName, outgoer.lastName, outgoer.pseudo, outgoer.address, outgoer.birthdate,
            outgoer.password, outgoer.gender, outgoer.FK_location, id],
            (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                    return;
                }

                if (res.affectedRows == 0) {
                    // not found Outgoer with the id
                    result({ kind: "not_found" }, null);
                    return;
                }

                console.log("updated outgoer: ", { id: id, ...outgoer });
                result(null, { id: id, ...outgoer });
            }
        );
    }
    static remove(id, result) {
        sql.query("DELETE FROM outgoers WHERE idOutgoer = ?", id, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Outgoer with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("deleted outgoer with id: ", id);
            result(null, res);
        });
    }
}

module.exports = Outgoer;
