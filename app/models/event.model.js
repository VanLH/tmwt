const sql = require("./db.js");

// constructor
class Event {
    constructor(event) {
        this.name = event.name;
        this.date = event.date;
        this.beginingHour = event.beginingHour;
        this.endingHour = event.endingHour;
        this.description = event.description;
        this.phone = event.phone;
        this.address = event.address;
        this.phone = event.phone;
        this.url = event.url;
        this.url = event.url;
        this.FK_location = event.FK_location;
        this.FK_welcomer = event.FK_welcomer;
        this.FK_eventKind = event.FK_eventKind;
    }
    static create(newEvent, result) {
        sql.query("INSERT INTO event SET ?", newEvent, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            console.log("created event: ", { id: res.insertId, ...newEvent });
            result(null, { id: res.insertId, ...newEvent });
        });
    }
    static findById(eventId, result) {
        sql.query(`SELECT *, DATE_FORMAT(date,'%d/%m/%Y') AS date FROM event WHERE idEvent = ${eventId}`, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if (res.length) {
                console.log("found event: ", res[0]);
                result(null, res[0]);
                return;
            }

            // not found Event with the id
            result({ kind: "not_found" }, null);
        });
    }
    static findAll({ name, kind, zip, musicStyle, generalStyle }, result) {
        let nameLike = ``;
        let selectClause = [`SELECT e.*, DATE_FORMAT(e.date,'%d/%m/%Y') AS 'date', 
                                CONCAT(e.beginingHour, ' ', e.endingHour) as 'hours', 
                                CONCAT(l.zipCode, ' ', l.city) AS 'place',
                                ek.name AS 'kind',
                                ms.name AS 'musicStyle',
                                gs.name AS 'generalStyle'`];
        const fromClause = `FROM event e`;
        let joinClause = [`JOIN eventkind ek ON e.FK_eventKind = ek.idEventKind 
                            JOIN locations l ON e.FK_Location = l.idLocation 
                            LEFT JOIN event_musicstyle ems ON ems.FK_Event = e.idEvent 
                            LEFT JOIN musicstyle ms ON ems.FK_Event = ms.idMusicStyle 
                            LEFT JOIN event_generalstyle egs ON e.idEvent = egs.FK_Event 
                            LEFT JOIN generalstyle gs ON egs.FK_Event = gs.idGeneralStyle`];
        let whereClause = [];

        if (name || kind || zip || musicStyle || generalStyle) {
            if (name) {
                nameLike = `%${name}%`;
                whereClause.push({ clause: `e.name LIKE ?`, value: nameLike, op: "AND" });
            }
            if (kind) {
                whereClause.push({ clause: `e.FK_EventKind = ?`, value: kind, op: 'AND' });
            }
            if (musicStyle) {
                whereClause.push({ clause: `ms.idMusicStyle = ?`, value: musicStyle, op: 'AND' });
            }
            if (generalStyle) {
                whereClause.push({ clause: `gs.idGeneralStyle = ?`, value: generalStyle, op: 'AND' });
            }
            if (zip) {
                whereClause.push({ clause: `e.FK_Location = ?`, value: zip, op: "AND" });
            }
        }
        let where = whereClause.length > 0 ? "WHERE " : "";
        const whereParam = [];

        for (const index in whereClause) {
            const clause = whereClause[index];
            if (whereClause.length > 1) {
                if (index != whereClause.length - 1) {
                    where += whereClause[index].clause + whereClause[index].op + " ";
                } else {
                    where += `${clause.clause}`;
                }
            }
            else {
                where += `${clause.clause}`;
            }

            whereParam.push(clause.value);
        }

        sql.query(`${selectClause} ${fromClause} ${joinClause} ${where}`, whereParam, (err, res) => {

            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            console.log("eventsAll: ", res);
            result(null, res);
        });
    }
    static updateById(id, event, result) {
        sql.query(
            `UPDATE event SET 
                name = ?, 
                date = ?, 
                beginingHour = ?, 
                endingHour = ?, 
                description = ?, 
                phone = ?, 
                address = ?, 
                url = ?, 
                FK_location = ?, 
                FK_welcomer = ?, 
                FK_eventKind  = ? 
            WHERE idEvent = ?`,
            [event.name, event.date, event.beginingHour, event.endingHour, event.description, event.phone,
            event.address, event.url, event.FK_location, event.FK_welcomer, event.FK_eventKind, id],
            (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                    return;
                }

                if (res.affectedRows == 0) {
                    // not found Event with the id
                    result({ kind: "not_found" }, null);
                    return;
                }

                console.log("updated event: ", { id: id, ...event });
                result(null, { id: id, ...event });
            }
        );
    }
    static remove(id, result) {
        sql.query("DELETE FROM event WHERE idEvent = ?", id, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Event with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("deleted event with id: ", id);
            result(null, res);
        });
    }
}

module.exports = Event;
