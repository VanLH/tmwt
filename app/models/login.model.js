const sql = require("./db.js");
const crypto = require('crypto');

class Login {

    static findOne({pseudo, password}, result) {
        let hash = crypto.createHash('sha512');
        //passing the data to be hashed
        let data = hash.update(password, 'utf-8');
        //Creating the hash in the required format
        let gen_hash = data.digest('hex');
        //Printing the output on the console
        password = gen_hash;
        console.log('password');

        sql.query(
            `SELECT 'outgoer' as 'typeOf',o.idOutgoers 
            FROM outgoers o 
            WHERE o.pseudo = ? AND o.password = ? 
            UNION 
            SELECT 'welcomer' as 'typeOf', w.idWelcomers 
            FROM welcomers w 
            WHERE w.pseudo = ? AND w.password = ?`,
            [pseudo, password, pseudo, password],
            (err, res) => {
                console.log('OK');

                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }

                if (res.length) {
                    console.log("found : ", res[0]);
                    result(null, res[0]);
                    return;
                }
                result({ kind: "not_found" }, null);
            });
    }
}

module.exports = Login;