const outgoers = require("../controllers/outgoer.controller.js");
module.exports = router => {
  
  router.route('/outgoers')
        .get(outgoers.findAll)  
        .post(outgoers.create);

    router.route('/outgoers/:outgoerId')
        .get(outgoers.findOne)
        .put(outgoers.update)
        .delete(outgoers.remove);
  }; 