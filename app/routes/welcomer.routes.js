const welcomers = require("../controllers/welcomer.controller.js");
module.exports = router => {

  router.route('/welcomers')
    .get(welcomers.findAll)
    .post(welcomers.create);

  router.route('/welcomers/:welcomerId')
    .get(welcomers.findOne)
    .put(welcomers.update)
    .delete(welcomers.remove); 
};
