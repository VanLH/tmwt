const express = require('express');
const router = express.Router();


require("./event.routes.js")(router);
require("./welcomer.routes.js")(router);
require("./outgoer.routes.js")(router);
require("./login.routes.js")(router);


module.exports = router;