const login = require("../controllers/login.controller.js");

module.exports = router => {

    router.route('/login')
        .get(login.findOne)
};