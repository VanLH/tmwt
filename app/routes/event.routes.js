const events = require("../controllers/event.controller.js");

module.exports = router => {
  
    router.route('/events')
        .get(events.findAll)  // Retrieve all Events / Argument '?name=...' for filter by event name
        .post(events.create); // Create a new Event

    router.route('/events/:eventId')
        .get(events.findOne)    // Retrieve a single Event with eventId
        .put(events.update)     // Update a Event with eventId
        .delete(events.remove); // Delete a Event with eventId
  };