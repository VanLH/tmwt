const Welcomer = require("../models/welcomer.model.js");

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {

        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    console.log(req.body);
    // Create a Welcomer
    const welcomer = new Welcomer({
        name: req.body.name,
        pseudo: req.body.pseudo,
        password: req.body.password,
        specality: req.body.specality,
        promo: req.body.promo,
        openingHour: req.body.openingHour,
        closingHour: req.body.closingHour,
        address: req.body.address,
        description: req.body.description,
        phone: req.body.phone,
        mobilePhone: req.body.mobilePhone,
        FK_location: req.body.FK_location,
        FK_typeWelcomer: req.body.FK_typeWelcomer,
    });
    
    // Save Welcomer in the database
    Welcomer.create(welcomer, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Welcomer."
            });
        else res.send(data);
    });
};

exports.findOne = (req, res) => {
    Welcomer.findById(req.params.welcomerId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Welcomer with id ${req.params.welcomerId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Welcomer with id " + req.params.welcomerId
                });
            }
        } else res.send(data);
    });
};

exports.findAll = (req, res) => {
    const { name, type, zip, typeCuisine  } = req.query;
    const callback = (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving welcomers."
            });
        else res.send(data);
    };

    Welcomer.findAll(req.query, (callback));
};

exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Welcomer.update(
        req.params.welcomerId,
        new Welcomer(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found Welcomer with id ${req.params.welcomerId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating Welcomer with id " + req.params.welcomerId
                    });
                }
            } else res.send(data);
        }
    );
};

exports.remove = (req, res) => {
    Welcomer.remove(req.params.welcomerId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Welcomer with id ${req.params.welcomerId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete Welcomer with id " + req.params.welcomerId
                });
            }
        } else res.send({ message: `Welcomer was deleted successfully!` });
    });
};