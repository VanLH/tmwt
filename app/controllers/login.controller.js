const Login = require("../models/login.model.js");

exports.findOne = (req, res) => {

    const callback = (err, data) => {
        if (err)
        if (err.kind === "not_found") {
            res.status(404).send({
                message: `Not found Outgoer with pseudo ${req.query.pseudo} and password ${req.query.password}.`
            });
        } else {
            res.status(500).send({
                message: "Error retrieving Outgoer with id " + req.params.outgoerId
            });
        }
        else res.send(data);
    };

    Login.findOne(req.query, (callback));
};

