const Event = require("../models/event.model.js");

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {

        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    console.log(req.body);
    // Create a Event
    const event = new Event({
        name: req.body.name,
        date: req.body.date,
        beginingHour: req.body.beginingHour,
        endingHour: req.body.endingHour,
        description: req.body.description,
        phone: req.body.phone,
        address: req.body.address,
        url: req.body.url,
        FK_location: req.body.FK_location,
        FK_welcomer: req.body.FK_welcomer,
        FK_eventKind: req.body.FK_eventKind,
    });

    // Save Event in the database
    Event.create(event, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Event."
            });
        else res.send(data);
    });
};

exports.findOne = (req, res) => {
    Event.findById(req.params.eventId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Event with id ${req.params.eventId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Event with id " + req.params.eventId
                });
            }
        } else res.send(data);
    });
};

exports.findAll = (req, res) => {
    const { name, kind, zip, musicStyle, generalStyle  } = req.query;
    console.log(req.query)
    const callback = (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving events."
            });
        else res.send(data);
    };

    Event.findAll(req.query, (callback));

};

exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Event.updateById(
        req.params.eventId,
        new Event(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found Event with id ${req.params.eventId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating Event with id " + req.params.eventId
                    });
                }
            } else res.send(data);
        }
    );
};

exports.remove = (req, res) => {
    Event.remove(req.params.eventId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Event with id ${req.params.eventId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete Event with id " + req.params.eventId
                });
            }
        } else res.send({ message: `Event was deleted successfully!` });
    });
};