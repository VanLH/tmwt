const Outgoer = require("../models/outgoer.model.js");

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    console.log(req.body);
    // Create a Outgoer
    const outgoer = new Outgoer({
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        pseudo : req.body.pseudo,
        address : req.body.address,
        birthdate : req.body.birthdate,
        password : req.body.password,
        gender : req.body.gender,
        FK_location : req.body.FK_location,
    });

    // Save Outgoer in the database
    Outgoer.create(outgoer, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Outgoer."
            });
        else res.send(data);
    });
};

exports.findOne = (req, res) => {
    Outgoer.findById(req.params.outgoerId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Outgoer with id ${req.params.outgoerId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Outgoer with id " + req.params.outgoerId
                });
            }
        } else res.send(data);
    });
};

exports.findAll = (req, res) => {
    const {id, name, firstname, pseudo, zip } = req.query;
    console.log(req.query)
    const callback = (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving outgoers."
            });
        else res.send(data);
    };

    Outgoer.findAll(req.query, (callback));
};

exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Outgoer.updateById(
        req.params.outgoerId,
        new Outgoer(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found Outgoer with id ${req.params.outgoerId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating Outgoer with id " + req.params.outgoerId
                    });
                }
            } else res.send(data);
        }
    );
};

exports.remove  = (req, res) => {
    Outgoer.remove(req.params.outgoerId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Outgoer with id ${req.params.outgoerId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete Outgoer with id " + req.params.outgoerId
                });
            }
        } else res.send({ message: `Outgoer was deleted successfully!` });
    });
};