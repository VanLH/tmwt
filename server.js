const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');

const app = express();


//gestion des cors
// app.use(cors);  //https://expressjs.com/en/resources/middleware/cors.html
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization")
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
  next()
})

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Tell Me Where To... application." });
});
 

// Add routing
const route = require('./app/routes/route');
app.use('/', route);


// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});
